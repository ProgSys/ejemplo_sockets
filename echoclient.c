#include "csapp.h"

int main(int argc, char **argv)
{
	int clientfd;
	char *port, filename[MAXLINE];
	char *host, buf[MAXLINE];
	rio_t rio;

	if (argc != 4) {
		fprintf(stderr, "usage: %s <host> <port> <filename>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];
	strcpy(filename,argv[3]);

	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);
	
	strcpy(buf, filename);


	Rio_writen(clientfd,buf,sizeof(buf));
	bzero((char *)&buf,sizeof(buf));
	Rio_readlineb(&rio,buf,sizeof(buf));
	
	memset(buf,0, sizeof buf);

	int val = rio_readn(clientfd, buf, 100);
	printf("%d\n", val);
	printf("Content:\n%s\n", buf);
	//printf("%dClose(clientfd);
	//exit(0);
}
