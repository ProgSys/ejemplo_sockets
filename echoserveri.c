#include "csapp.h"

void echo(int connfd);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		echo(connfd);
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	size_t no;
	char buf[MAXLINE];
	rio_t rio;
	char* response;

	Rio_readinitb(&rio, connfd);
	int n = rio_readn(connfd, buf, MAXLINE); 
	printf("Buscando %s ...\n", buf);

	FILE * searched;
	int nbytes;

	searched = fopen(buf, "r+");

	if(searched!=NULL){
		/* open an existing file for reading */
		/* Get the number of bytes */
		fseek(searched, 0L, SEEK_END);
		nbytes = ftell(searched) ;
		 

		char* buffer = (char*)calloc(nbytes, sizeof(char));	


		/* reset the file position indicator to 
		the beginning of the file */
		fseek(searched, 0L, SEEK_SET);	
		 
		
		/* memory error */
		if(buffer != NULL){
			/* copy all the text into the buffer */
			int bWritten= fread(buffer, sizeof(char), nbytes, searched);
			fclose(searched);
			 
			/* confirm we have read the file by
			outputing it to the console */
			printf("The file called contains this num of bytes %d\n\n", bWritten);
			rio_writen(connfd,buffer,strlen(buffer));
			printf("%s\n", buffer);
			free(buffer);
		}
		else{
			response = "Empty file!";
			rio_writen(connfd, response, strlen(response));
			printf("%s\n", response );
		}
	}else{
		response = "File does not exist!";
		rio_writen(connfd, response, strlen(response));
		printf("%s\n", response );
	}

	memset(buf, 0, sizeof(buf));

	//	printf("server received %lu bytes\n", n);
	//	Rio_writen(connfd, buf, n);
	//}

}
